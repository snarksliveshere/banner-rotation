.PHONY: up down run

run:
	docker-compose up -d --build

up:
	docker-compose up -d --build

down:
	docker-compose down --remove-orphans

restart: down up

test-unit:
	set -e ;\
	test_status_code=0 ;\
	docker-compose -f docker-compose.test.yml run grpc_server_unit_test go test -v -race || test_status_code=$$? ;\
	docker-compose -f docker-compose.test.yml down ;\
	echo $$test_status_code;\
	exit $$test_status_code ;\

test-integration:
	set -e ;\
	docker-compose up --build -d ;\
	test_status_code=0 ;\
	docker-compose -f docker-compose.test.yml run grpc_client go test || test_status_code=$$? ;\
	docker-compose -f docker-compose.test.yml down ;\
	echo $$test_status_code;\
	exit $$test_status_code ;\

test-db-integration:
	set -e ;\
	docker-compose up --build -d ;\
	test_status_code=0 ;\
	docker-compose -f docker-compose.test.yml run grpc_server_unit_test go test ../db_integration || test_status_code=$$? ;\
	docker-compose -f docker-compose.test.yml down ;\
	echo $$test_status_code;\
	exit $$test_status_code ;\

test-all:
	set -e ;\
	docker-compose up --build -d ;\
	test_status_code=0 ;\
	docker-compose -f docker-compose.test.yml run grpc_server_unit_test go test || test_status_code=$$? ;\
	docker-compose -f docker-compose.test.yml run grpc_server_unit_test go test ../db_integration || test_status_code=$$? ;\
	docker-compose -f docker-compose.test.yml run grpc_client go test || test_status_code=$$? ;\
	docker-compose -f docker-compose.test.yml down ;\
	echo $$test_status_code;\
	exit $$test_status_code ;\



# service commands
rebuild-unit-test:
	docker-compose -f docker-compose.test.yml up --build --no-deps -d grpc_server_unit_test

rebuild-client:
	docker-compose -f docker-compose.test.yml up --build --no-deps -d grpc_client

run-unit-test:
	docker-compose -f docker-compose.test.yml run grpc_server_unit_test go test -v -race || test_status_code=$$? ;\

example:
	docker-compose -f docker-compose.test.yml up --build -d

rebuild-grpc-server:
	docker-compose -f docker-compose.test.yml up --build --no-deps -d grpc_server