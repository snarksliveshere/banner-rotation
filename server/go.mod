module gitlab.com/snarksliveshere/banner-rotation/server

go 1.13

require (
	github.com/go-pg/pg v8.0.6+incompatible
	github.com/golang/protobuf v1.3.3
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/streadway/amqp v0.0.0-20200108173154-1c71cc93ed71
	go.uber.org/zap v1.13.0
	google.golang.org/grpc v1.27.1
	mellium.im/sasl v0.2.1 // indirect
)
