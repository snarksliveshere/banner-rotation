package core_test

import (
	"log"
	"math/rand"
	"testing"
	"time"

	"gitlab.com/snarksliveshere/banner-rotation/server/internal/database/models"
	"gitlab.com/snarksliveshere/banner-rotation/server/internal/task"
)

// тестируем логику после большого количества показов, каждый баннер должен быть показан хотя один раз.
// тестируется, что все баннеры из набора будут показаны, даже крайне непопулярные
func TestGetBanner(t *testing.T) {
	banners := prepareBanners()
	control := prepareBanners()

	for i := 0; i < 1000000; i++ {
		banner, err := task.GetBanner(&banners)
		if err != nil {
			log.Fatal(err)
		}
		var rew bool
		if randomClick() {
			rew = true
		}
		incBannerStatistics(&banners, banner.ID, rew)
	}

	for k, v := range banners.Banners {
		if v.Shows == control.Banners[k].Shows {
			t.Errorf("TestGetBanner(), equal shows on distance:%d\n", v.Shows)
		}
	}
}

//Выбор популярных: если на один из баннеров кликают, у него должно быть существенно больше показов чем у остальных.
// я ставлю коеффициент 0.2 на баннер become_popular,в отличии от 0.05
//так как несколько абстрактно, что есть "существенно больше", то поставлю в 10 раз
func TestGetBannerStartPopular(t *testing.T) {
	banners := prepareBanners()
	popularBannerID := "become_popular"
	for i := 0; i < 1000000; i++ {
		banner, err := task.GetBanner(&banners)
		if err != nil {
			log.Fatal(err)
		}
		var rew bool
		coeff := 0.05
		if banner.ID == popularBannerID {
			coeff = 0.2
		}
		if notRandomClick(coeff) {
			rew = true
		}
		incBannerStatistics(&banners, banner.ID, rew)
	}

	var showsMostClicksBanner int
	for _, v := range banners.Banners {
		if v.ID == popularBannerID {
			showsMostClicksBanner = v.Shows
			break
		}
	}
	res := true
	for _, v := range banners.Banners {
		if v.ID == popularBannerID {
			continue
		}
		if showsMostClicksBanner/v.Shows < 10 {
			res = false
		}
	}
	if !res {
		t.Errorf("TestGetBannerStartPopular(), most popular banner must have shows 4 times more")
	}
}

func TestGetBanners(t *testing.T) {
	bStat, count := prepareSliceStatisticsModel()
	bS := task.GetBanners(bStat)
	if int(count) != bS.Count {
		t.Errorf("TestGetBanners(), count not equal=init:%v,given:%v\n", count, bS.Count)
	}
	if len(bStat) != len(bS.Banners) {
		t.Errorf("TestGetBanners(), length not equal=init:%v,given:%v\n", len(bStat), len(bS.Banners))
	}
}

func prepareBanners() task.Banners {
	b1 := task.Banner{
		ID:     "popular_banner_1",
		Shows:  150,
		Clicks: 20,
	}
	b2 := task.Banner{
		ID:     "popular_banner_2",
		Shows:  400,
		Clicks: 60,
	}
	b3 := task.Banner{
		ID:     "low_banner_1",
		Shows:  400,
		Clicks: 5,
	}
	b4 := task.Banner{
		ID:     "extra_low_banner_1",
		Shows:  800,
		Clicks: 0,
	}
	b5 := task.Banner{
		ID:     "extra_low_banner_2",
		Shows:  1500,
		Clicks: 0,
	}
	b6 := task.Banner{
		ID:     "become_popular",
		Shows:  1500,
		Clicks: 0,
	}

	bs := make([]task.Banner, 0, 5)
	bs = append(bs, b1, b2, b3, b4, b5, b6)

	var count int
	for _, v := range bs {
		count += v.Shows
	}

	return task.Banners{
		Banners: bs,
		Count:   count,
	}
}

func incBannerStatistics(banners *task.Banners, id string, rew bool) {
	for k, v := range banners.Banners {
		if v.ID == id {
			tr := v.Shows + 1
			b := task.Banner{
				ID:     v.ID,
				Shows:  tr,
				Clicks: v.Clicks,
			}
			if rew {
				r := v.Clicks + 1
				b.Clicks = r
			}
			banners.Banners[k] = b
		}
	}
	banners.Count++
}

func randomClick() bool {
	b := getRandomFloat()
	return b < 0.05
}

func notRandomClick(coefficient float64) bool {
	b := getRandomFloat()
	return b < coefficient
}

func getRandomFloat() float64 {
	rand.Seed(time.Now().UnixNano())
	return rand.Float64()
}

func prepareSliceStatisticsModel() (stats []*models.Statistics, count uint64) {
	s1 := models.Statistics{
		BannerID: "banner1",
		Clicks:   45,
		Shows:    150,
	}
	s2 := models.Statistics{
		BannerID: "banner2",
		Clicks:   10,
		Shows:    250,
	}
	s3 := models.Statistics{
		BannerID: "banner3",
		Clicks:   2,
		Shows:    100,
	}
	stats = append(stats, &s1, &s2, &s3)
	for _, v := range stats {
		count += v.Shows
	}
	return
}
