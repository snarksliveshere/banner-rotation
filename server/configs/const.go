package configs

const (
	EventTimeLayout            = "2006-01-02T15:04:05-0700"
	ProtoResponseStatusDefault = 0
	ProtoResponseStatusSuccess = 1
	ProtoResponseStatusError   = 2
	BannerStatQueue            = "banner_statistics"
	BannerStatClick            = "click"
	BannerStatShow             = "show"
	BannerStatEx               = "stat_banner"
)
