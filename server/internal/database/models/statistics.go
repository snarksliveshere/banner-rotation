package models

type Statistics struct {
	TableName  struct{} `sql:"statistics"`
	AudienceID string   `sql:"audience_id,notnull" pg:"unique:public_statistics__uidx"`
	BannerID   string   `sql:"banner_id,notnull" pg:"unique:public_statistics_uidx"`
	SlotID     string   `sql:"slot_id,notnull" pg:"unique:public_statistics_uidx"`
	Clicks     uint64   `sql:"clicks,use_zero,notnull"`
	Shows      uint64   `sql:"shows,use_zero,notnull"`
}
