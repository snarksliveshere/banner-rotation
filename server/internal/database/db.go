package database

import (
	"sync"

	"gitlab.com/snarksliveshere/banner-rotation/server/configs"

	"github.com/go-pg/pg"
)

var dbOnce sync.Once
var dbConn *pg.DB

type DB struct {
	Conf *configs.AppConfig
}

func (db *DB) CreatePgConn() *pg.DB {
	opt := &pg.Options{
		Addr:     db.Conf.DBHost + ":" + db.Conf.DBPort,
		User:     db.Conf.DBUser,
		Password: db.Conf.DBPassword,
		Database: db.Conf.DBName,
	}

	dbOnce.Do(func() {
		dbConn = pg.Connect(opt)
	})
	return dbConn
}
