package task

import (
	"encoding/json"
	"time"

	"github.com/go-pg/pg"
	"github.com/streadway/amqp"
	"gitlab.com/snarksliveshere/banner-rotation/server/configs"
	"gitlab.com/snarksliveshere/banner-rotation/server/internal/database"
	"gitlab.com/snarksliveshere/banner-rotation/server/internal/database/models"
	"go.uber.org/zap"
)

type Banner struct {
	ID     string
	Shows  int
	Clicks int
}

type BannerStat struct {
	ID       int
	Trials   int
	Reward   int
	Slot     int
	Audience int
}

type Banners struct {
	Banners []Banner
	Count   int
}

type BannerStatistics struct {
	Type     string `json:"type"`
	Slot     string `json:"slot"`
	Audience string `json:"audience"`
	Banner   string `json:"banner"`
	Time     string `json:"time"`
}

func ReturnBanner(db *pg.DB, slog *zap.SugaredLogger, channel *amqp.Channel, audience, slot string) (string, error) {
	bannersRows, err := database.GetBannerStat(db, audience, slot)
	if err != nil {
		return "", err
	}
	banners := GetBanners(bannersRows)
	if err != nil {
		return "", err
	}
	banner, err := GetBanner(&banners)
	if err != nil {
		return "", err
	}
	row := &models.Statistics{
		AudienceID: audience,
		BannerID:   banner.ID,
		SlotID:     slot,
		Clicks:     uint64(banner.Clicks),
		Shows:      uint64(banner.Shows) + 1,
	}
	err = database.InsertRowIntoStat(db, row)
	if err != nil {
		return "", err
	}
	statToRabbit := &BannerStatistics{
		Type:     configs.BannerStatShow,
		Slot:     slot,
		Audience: audience,
		Banner:   banner.ID,
		Time:     time.Now().Format(configs.EventTimeLayout),
	}

	data, err := json.Marshal(statToRabbit)
	if err != nil {
		return "", err
	}
	err = bannerStatToRabbit(channel, configs.BannerStatQueue, data)
	if err != nil {
		return "", err
	}
	return banner.ID, nil
}

func AddClickToBanner(db *pg.DB, channel *amqp.Channel, banner, slot, audience string) error {
	err := database.AddClick(db, banner, slot, audience)
	if err != nil {
		return err
	}
	statToRabbit := &BannerStatistics{
		Type:     configs.BannerStatClick,
		Slot:     slot,
		Audience: audience,
		Banner:   banner,
		Time:     time.Now().Format(configs.EventTimeLayout),
	}
	data, err := json.Marshal(statToRabbit)
	if err != nil {
		return err
	}
	err = bannerStatToRabbit(channel, configs.BannerStatQueue, data)
	if err != nil {
		return err
	}
	return nil
}

func AddBannerToSlot(db *pg.DB, banner, slot string) error {
	err := database.AddBannerToSlot(db, banner, slot)
	if err != nil {
		return err
	}
	return nil
}

func DeleteBannerFromSlot(db *pg.DB, banner, slot string) error {
	err := database.DeleteBannerFromSlot(db, banner, slot)
	if err != nil {
		return err
	}
	return nil
}

func bannerStatToRabbit(ch *amqp.Channel, rk string, stat []byte) error {
	err := ch.Publish(
		configs.BannerStatEx, // exchange
		rk,
		false,
		false,
		amqp.Publishing{
			DeliveryMode: amqp.Persistent,
			ContentType:  "text/plain",
			Body:         stat,
		})
	if err != nil {
		return err
	}
	return nil
}

func GetBanners(loadedRows []*models.Statistics) Banners {
	bsInit := make([]Banner, 0, len(loadedRows))
	var count int
	for _, v := range loadedRows {
		count += int(v.Shows)
		b := Banner{
			ID:     v.BannerID,
			Shows:  int(v.Shows),
			Clicks: int(v.Clicks),
		}
		bsInit = append(bsInit, b)
	}
	return Banners{Count: count, Banners: bsInit}
}
