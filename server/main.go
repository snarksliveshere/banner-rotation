package main

import (
	"log"
	"os"
	"os/signal"
	"syscall"

	"github.com/kelseyhightower/envconfig"
	"gitlab.com/snarksliveshere/banner-rotation/server/cmd/grpc"
	"gitlab.com/snarksliveshere/banner-rotation/server/cmd/rabbit"
	"gitlab.com/snarksliveshere/banner-rotation/server/configs"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

func failOnError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)
	}
}

func main() {
	var conf configs.AppConfig
	failOnError(envconfig.Process("grpc_server", &conf), "failed to init config")

	stopCh := make(chan os.Signal, 1)
	signal.Notify(stopCh, syscall.SIGINT, syscall.SIGTERM)
	slog := loggerInit()
	rabbitConn := rabbit.RbCreateConnection(conf, slog)
	defer func() { _ = rabbitConn.Close() }()
	rabbitChannel := rabbit.RbCreateChannel(rabbitConn)
	defer func() { _ = rabbitChannel.Close() }()
	go func() { grpc.Server(conf, slog, rabbitChannel) }()
	go func() { rabbit.RbCreateServer(rabbitChannel) }()

	<-stopCh
}

func loggerInit() *zap.SugaredLogger {
	cfg := zap.NewDevelopmentConfig()
	cfg.EncoderConfig.EncodeLevel = customLevelEncoder
	logger, err := cfg.Build()
	if err != nil {
		log.Fatal(err.Error())
	}
	slog := logger.Sugar()
	defer func() { _ = slog.Sync() }()
	slog.Info("GRPC Server Starts")
	return slog
}

func customLevelEncoder(level zapcore.Level, enc zapcore.PrimitiveArrayEncoder) {
	enc.AppendString("[" + level.CapitalString() + "]")
}
