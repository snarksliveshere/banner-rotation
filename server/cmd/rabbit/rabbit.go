package rabbit

import (
	"log"
	"time"

	"github.com/streadway/amqp"
	"go.uber.org/zap"

	"gitlab.com/snarksliveshere/banner-rotation/server/configs"
)

func RbCreateServer(channel *amqp.Channel) {
	rabbitServer(channel)
}

func RbCreateConnection(conf configs.AppConfig, slog *zap.SugaredLogger) *amqp.Connection {
	return createRabbitConn(conf, slog)
}

func RbCreateChannel(conn *amqp.Connection) *amqp.Channel {
	return createChannel(conn)
}

func createRabbitConn(conf configs.AppConfig, slog *zap.SugaredLogger) *amqp.Connection {
	strDial := "amqp://" + conf.RabbitUser + ":" + conf.RabbitPassword + "@" + conf.RabbitHost + ":" + conf.RabbitPort + "/"
	for {
		conn, err := amqp.Dial(strDial)
		if err == nil {
			return conn
		}
		slog.Infof("INFO:Failed to connect to RabbitMQ with error:%s and strDial:%s", err.Error(), strDial)
		time.Sleep(3 * time.Second)
	}
}

func rabbitServer(ch *amqp.Channel) {
	err := ch.ExchangeDeclare(
		configs.BannerStatEx,
		"fanout",
		true,
		false,
		false,
		false,
		nil,
	)
	failOnError(err, "Failed to declare an exchange")

	_, err = ch.QueueDeclare(
		configs.BannerStatQueue,
		true,
		false,
		false,
		false,
		nil,
	)
	failOnError(err, "Failed to declare a queue")
	err = ch.QueueBind(configs.BannerStatQueue, "", configs.BannerStatEx, false, nil)

	failOnError(err, "Failed to bind a queue")
}

func createChannel(conn *amqp.Connection) *amqp.Channel {
	ch, err := conn.Channel()
	failOnError(err, "Failed to open a channel")
	return ch
}

func failOnError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)
	}
}
