package configs

type Addr struct {
	GRPCPort string `envconfig:"GRPC_PORT" required:"true"`
	ListenIP string `envconfig:"LISTEN_IP"`
}

type Rabbit struct {
	RabbitHost     string `envconfig:"RABBIT_HOST" required:"true"`
	RabbitPort     string `envconfig:"RABBIT_PORT" required:"true"`
	RabbitUser     string `envconfig:"RABBIT_USER" required:"true"`
	RabbitPassword string `envconfig:"RABBIT_PASSWORD" required:"true"`
}

type AppConfig struct {
	Addr
	Rabbit
	LogLevel string `envconfig:"LOG_LEVEL" required:"true"`
}
