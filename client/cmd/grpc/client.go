package grpc

import (
	"context"

	"gitlab.com/snarksliveshere/banner-rotation/client/api/proto"
	"gitlab.com/snarksliveshere/banner-rotation/client/configs"
	"go.uber.org/zap"
	"google.golang.org/grpc"
)

type Conn struct {
	GConn  *grpc.ClientConn
	Client proto.BannerServiceClient
	Ctx    context.Context
	log    *zap.SugaredLogger
}

func Client(conf configs.AppConfig, log *zap.SugaredLogger) *Conn {
	cc, err := grpc.Dial(conf.ListenIP+":"+conf.GRPCPort, grpc.WithInsecure())
	if err != nil {
		log.DPanic(err.Error())
		log.Fatalf("could not connect: %v", err)
	}
	client := proto.NewBannerServiceClient(cc)
	return &Conn{
		GConn:  cc,
		Client: client,
		Ctx:    context.Background(),
		log:    log,
	}
	// здесь какая-нибудь логика. т.к. мне клиент нужен только для интеграционных тестов, я ее не реализую
}

func (g *Conn) GetHealthCheck(msg proto.Empty) (*proto.ResponseBannerMessage, error) {
	defer func() { _ = g.GConn.Close() }()
	resp, err := g.Client.SendHealthCheckMessage(g.Ctx, &msg)
	if err != nil {
		return nil, err
	}
	return resp, nil
}

func (g *Conn) GetBanner(msg proto.GetBannerRequestMessage) (*proto.GetBannerResponseMessage, error) {
	defer func() { _ = g.GConn.Close() }()
	resp, err := g.Client.SendGetBannerMessage(g.Ctx, &msg)
	if err != nil {
		return nil, err
	}
	return resp, nil
}

func (g *Conn) AddClick(msg proto.AddClickRequestMessage) (*proto.ResponseBannerMessage, error) {
	defer func() { _ = g.GConn.Close() }()
	resp, err := g.Client.SendAddClickBannerMessage(g.Ctx, &msg)
	if err != nil {
		return nil, err
	}
	return resp, nil
}

func (g *Conn) AddBannerToSlot(msg proto.AddBannerToSlotRequestMessage) (*proto.ResponseBannerMessage, error) {
	defer func() { _ = g.GConn.Close() }()
	resp, err := g.Client.SendAddBannerToSlotMessage(g.Ctx, &msg)
	if err != nil {
		return nil, err
	}
	return resp, nil
}

func (g *Conn) DeleteBannerFromSlot(msg proto.DeleteBannerFromSlotRequestMessage) (*proto.ResponseBannerMessage, error) {
	defer func() { _ = g.GConn.Close() }()
	resp, err := g.Client.SendDeleteBannerFromSlotMessage(g.Ctx, &msg)
	if err != nil {
		return nil, err
	}
	return resp, nil
}

//protoc ./proto/events.proto --go_out=plugins=grpc:.
